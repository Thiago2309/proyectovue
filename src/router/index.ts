import { createRouter, createWebHistory } from 'vue-router'
// import HomeView from '../views/HomeView.vue'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import RegisterView from '../views/RegisterView.vue'
// import AboutView from '../views/AboutView.vue'
import DashboardView from '../views/DashboardView.vue'
import GastoView from '../views/GastoView.vue'
import IngresoView from '../views/IngresoView.vue'
import AhorroView from '../views/AhorroView.vue'
import PerfilView from '@/views/PerfilView.vue'
import NewIngresoView from '@/views/NewIngresoView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/home',
      name: 'home',
      component: HomeView,
      children: [
        {
          path: '',
          name: 'test',
          component: DashboardView
        },
        {
          path: 'Gasto',
          name: 'gasto',
          component: GastoView
        },
        {
          path: 'Ingreso',
          name: 'ingreso',
          component: IngresoView
        },
        {
          path: 'Ahorro',
          name: 'ahorro',
          component: AhorroView
        },
        {
          path: 'Perfil',
          name: 'perfil',
          component: PerfilView
        },
        {
          path: 'NuevoIngreso',
          name: 'NewIngreso',
          component: NewIngresoView
        }
      ]
    },
    {
      path: '/',
      name: 'login',
      component: LoginView
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView
    }
  ]
})

export default router
