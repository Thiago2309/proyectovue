export const data = {
  labels: ['Sueldo', 'Regalo'],
  datasets: [
    {
      backgroundColor: ['#41B883', '#E46651'],
      data: [80, 20]
    }
  ]
}

export const options = {
  responsive: true,
  maintainAspectRatio: false
}
